SpeciesFinder
===================

This project documents the VirulenceFinder service


Documentation
=============

The SpeciesFinder service contains one python script speciesfinder.py which is the script of the latest
version of the SpeciesFinder service. SpeciesFinder identifies the specie of the sequenced data from the
16s RNA gene.


## Content of the repository
1. speciesfinder.py      - the program
2. test     	- test folder
3. README.md


## Installation

Setting up SpeciesFinder program   
**Warning:** Due to bugs in BioPython 1.74, do not use that version if not using Python 3.7.
```bash
# Go to wanted location for speciesfinder
cd /path/to/some/dir
# Clone and enter the speciesfinder directory
git clone https://bitbucket.org/genomicepidemiology/speciesfinder.git
cd speciesfinder
```

#Download and install SpeciesFinder database

```bash
# Go to the directory where you want to store the speciesfinder database
cd /path/to/some/dir
# Clone database from git repository
git clone https://bitbucket.org/genomicepidemiology/speciesfinder_db.git
cd speciesfinder_db
SPECIESFINDER_DB=$(pwd)
# Install VirulenceFinder database with executable kma_index program
python3 INSTALL.py kma_index
```

If kma_index has no bin install please install kma_index from the kma repository:
https://bitbucket.org/genomicepidemiology/kma

## Dependencies
In order to run the program, Python 3.5 (or newer) should be installed along with the following versions of the modules (or newer).

#### Modules
- cgecore 1.5.5
- tabulate 0.7.7
- pandas 0.17.1

Modules can be installed using the following command. Here, the installation of the module cgecore is used as an example:
```bash
pip3 install cgecore
```
#### KMA
Additionally KMA should be installed.
The newest version of KMA can be installed from here:
```url
https://bitbucket.org/genomicepidemiology/kma
```

## Usage

The program can be invoked with the -h option to get help and more information of the service.





`-i INPUTFILE	input file (fasta or fastq) relative to pwd, up to 2 files`

`-o OUTDIR	output directory relative to pwd`

`-p DATABASE_PATH    set path to database`

`-kp METHOD_PATH    set path to kma`

`-tmp    temporary directory for storage of the results from the external software`

`-q    don't show results `

## Web-server

A webserver implementing the methods is available at the [CGE website](http://www.genomicepidemiology.org/) and can be found here:
https://cge.cbs.dtu.dk/services/SpeciesFinder/

Citation
=======

When using the method please cite:

Philip T.L.C. Clausen, Frank M. Aarestrup & Ole Lund, "Rapid and precise alignment of raw reads against redundant databases with KMA", BMC Bioinformatics, 2018;19:307.


License
=======

Copyright (c) 2014, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
